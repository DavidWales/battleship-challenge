from .coordinate import Coordinate, deserialize_coordinate

class PlayerInfo():
    start_coordinate: Coordinate = None
    end_coordinate: Coordinate = None
    player_name: str = None
    ships_sunk: int = None
    fleet_sunk: bool = None

    def __str__(self):
        return f"<PlayerInfo: {self.start_coordinate.__str__()} {self.end_coordinate.__str__()} \
            {self.player_name} {self.ships_sunk} {self.fleet_sunk}>"

    def deserialize(self, data):
        self.start_coordinate = deserialize_coordinate(data.get("startCoordinate"))
        self.end_coordinate = deserialize_coordinate(data.get("endCoordinate"))
        self.player_name = data.get("playerName")
        self.ships_sunk = data.get("shipsSunk")
        self.fleet_sunk = data.get("fleetSunk")

def deserialize_player_info(player_json):
    player = PlayerInfo()
    player.deserialize(player_json)
    return player
