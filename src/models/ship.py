from .coordinate import Coordinate

class Ship():
    fleet_name: str = None
    damage: bool = []
    position_relative: Coordinate = Coordinate(0, 0)
    position_absolute: Coordinate = Coordinate(0, 0)
    id: str = None
    orientation: int = None
    length: int = None
    maximum_pending_orders: int = None
    can_perform_recon: bool = None

    def __init__(self, fleet_name, length):
        self.fleet_name = fleet_name
        self.length = length
        self.damage = [False for i in range(self.length)]

    def is_sunk(self):
        return all(damaged for damaged in self.damage)

    def serialize(self):
        return {
            "FleetName": self.fleet_name,
            "Damage": self.damage,
            "PositionRelative": self.position_relative.serialize(),
            "PositionAbsolute": self.position_absolute.serialize(),
            "ID": self.id,
            "Orientation": self.orientation,
            "Length": self.length,
            "MaximumPendingOrders": self.maximum_pending_orders,
            "CanPerformRecon": self.can_perform_recon
        }
