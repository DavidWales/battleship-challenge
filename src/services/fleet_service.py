from random import randint
from jsonpickle import encode
from .state_service import StateService
from .storage_service import StorageService
from ..models import Fleet, Ship, Coordinate
from ..constants import Orientation

class FleetService():
    def __init__(self):
        self.storage_service = StorageService()
        self.state_service = StateService()

    def get_fleet(self):
        fleet = Fleet(self.state_service.state.fleet_name)

        fleet.aircraft_carrier = Ship("ac", 5)
        self._set_ship_position(fleet.aircraft_carrier, 3, 3, Orientation.vertical)

        fleet.battleship = Ship("bs", 4)
        self._set_ship_position(fleet.battleship, 13, 7, Orientation.horizontal)

        fleet.destroyer = Ship("d", 4)
        self._set_ship_position(fleet.destroyer, 5, 11, Orientation.horizontal)

        fleet.pt_boat = Ship("ptb", 2)
        self._set_ship_position(fleet.pt_boat, 10, 10, Orientation.vertical)

        fleet.submarine = Ship("sub", 3)
        self._set_ship_position(fleet.submarine, 14, 13, Orientation.vertical)

        self._persist_fleet(fleet)
        return fleet

    def _persist_fleet(self, fleet):
        self.storage_service.set_val("fleet", encode(fleet))

    def _set_ship_position(self, ship, x, y, orient):
        ship.position_relative = Coordinate(x, y)
        ship.orientation = orient

    def _get_random_position(self, min_x, max_x, min_y, max_y):
        return Coordinate(randint(min_x, max_x), randint(min_y, max_y))
