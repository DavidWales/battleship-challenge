from enum import IntEnum

class OrderType(IntEnum):
    attack = 0
    recon = 1
    broadcast_message = 2
