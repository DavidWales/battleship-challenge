from ..constants import StateStatus

class State():
    game_info = None
    recon_coordinate = None
    board = []
    fleet = None
    fleet_name = None

    def to_html(self):
        return self._get_game_info_html() + self._get_player_info_html()

    def _get_game_info_html(self):
        return f"""
            Game Info<br/>
            =======================<br/>
            Battlefield Width: {self.game_info.battlefield_width}<br/>
            Battlefield Height: {self.game_info.battlefield_height}<br/><br/>
        """

    def _get_player_info_html(self):
        player_info_segment = f"""
            Player Info<br/>
            =======================<br/>
        """
        for p_info in self.game_info.players:
            player_info_segment += f"""
                {p_info.player_name}: Start Coord ({p_info.start_coordinate.x}, {p_info.start_coordinate.y}), End Coord ({p_info.end_coordinate.x}, {p_info.end_coordinate.y})<br/>
                {self._get_player_board(p_info.start_coordinate.x, p_info.start_coordinate.y, p_info.end_coordinate.x, p_info.end_coordinate.y)}<br><br>
            """
        return player_info_segment + "<br/><br/>"

    def _get_player_board(self, start_x, start_y, end_x, end_y):
        p_board = [row[start_x:end_x] for row in self.board[start_y:end_y]]
        board_segment = "<table>"
        for row in p_board:
            board_segment += "<tr>"
            for x in row:
                board_segment += f"<td>{_get_state_status_char(x)}</td>"
            board_segment += "</tr>"
        return board_segment + "</table>"

def _get_state_status_char(status):
    mappings = {
        StateStatus.UNKNOWN: '?',
        StateStatus.EMPTY: ' ',
        StateStatus.SHIP: 'o',
        StateStatus.SHIP_HIT: 'x',
        StateStatus.SHIP_SUNK: '-'
    }
    return mappings[status]
