class Fleet():
    name = None
    aircraft_carrier = None
    battleship = None
    destroyer = None
    pt_boat = None
    submarine = None

    def __init__(self, name):
        self.name = name

    def serialize(self):
        return {
            "Name": self.name,
            "AircraftCarrier": self.aircraft_carrier.serialize(),
            "Battleship": self.battleship.serialize(),
            "Destroyer": self.destroyer.serialize(),
            "PTBoat": self.pt_boat.serialize(),
            "Submarine": self.submarine.serialize()
        }
