import os
from jsonpickle import encode, decode
from .storage_service import StorageService
from ..models import State, Coordinate
from ..constants import StateStatus

class StateService():
    def __init__(self, game_info=None):
        self.storage_service = StorageService()
        self._init_state(game_info)

    def persist_state(self):
        self.storage_service.set_val('gameinfo', encode(self.state.game_info))
        self.storage_service.set_val('board', encode(self.state.board))
        self.storage_service.set_val("fleetname", self.state.fleet_name)
        self.storage_service.set_val("reconcoord", encode(self.state.recon_coordinate))

    def set_fleet(self):
        fleet = decode(self.storage_service.get_val("fleet"))
        player = self.get_player(fleet.name)
        ships = [fleet.aircraft_carrier, fleet.battleship, fleet.destroyer, \
            fleet.pt_boat, fleet.submarine]
        for ship in ships:
            x = ship.position_relative.x + player.start_coordinate.x
            y = ship.position_relative.y + player.start_coordinate.y
            for _ in range(ship.length):
                self.update_board(x, y, StateStatus.SHIP, persist=False)
                if ship.orientation == 0:
                    x += 1
                else:
                    y += 1
        self.persist_state()

    def get_player(self, fleet_name):
        for player in self.state.game_info.players:
            if player.player_name == fleet_name:
                return player
        return None

    def update_board(self, x, y, mark, persist=True):
        if y >= len(self.state.board) or x >= len(self.state.board[y]):
            #Coordinate is off the board
            return
        self.state.board[y][x] = mark
        if persist is True:
            self.persist_state()

    def _init_state(self, game_info):
        self.state = State()
        if game_info is not None:
            self._create_state(game_info)
        else:
            self._get_state()

    def _get_state(self):
        self.state.board = decode(self.storage_service.get_val('board'))
        self.state.game_info = decode(self.storage_service.get_val('gameinfo'))
        self.state.fleet = decode(self.storage_service.get_val("fleet"))
        self.state.fleet_name = self.storage_service.get_val("fleetname").decode("utf-8")
        self.state.recon_coordinate = decode(self.storage_service.get_val("reconcoord"))

    def _create_state(self, game_info):
        self.state.board = [[StateStatus.UNKNOWN for col in range(game_info.battlefield_width)] \
                for row in range(game_info.battlefield_height)]
        self.state.game_info = game_info
        self.state.fleet_name = os.getenv("FLEET_NAME")
        self.state.recon_coordinate = Coordinate(-1, -1)
        self.persist_state()
