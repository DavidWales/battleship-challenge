class Coordinate():
    x: int = 0
    y: int = 0

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def equals(self, other_coordinate):
        return self.x == other_coordinate.x and self.y == other_coordinate.y

    def __str__(self):
        return f"<Coordinate: {self.x} {self.y}>"

    def serialize(self):
        return {
            "X": self.x,
            "Y": self.y
        }

    def deserialize(self, data):
        self.x = data.get("X", 0)
        self.y = data.get("Y", 0)

def deserialize_coordinate(data):
    coord = Coordinate(None, None)
    coord.deserialize(data)
    return coord
