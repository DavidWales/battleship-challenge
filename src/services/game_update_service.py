from .storage_service import StorageService
from .state_service import StateService
from ..constants import OrderType, OrderResult, StateStatus

class GameUpdateService():
    def __init__(self):
        self.storage_service = StorageService()
        self.state_service = StateService()

    def update(self, order):
        self.update_state(order)
        #print(f"Order: {order.player_targeted}, Mine: {state.fleet_name}")

    def update_state(self, order):
        if order.order_type is int(OrderType.attack):
            self._handle_attack_action(order)
        elif order.order_type is int(OrderType.recon):
            self._handle_recon_action(order)

    def _handle_attack_action(self, order):
        x = order.coordinate.x
        y = order.coordinate.y
        if order.result is int(OrderResult.miss):
            self.state_service.update_board(x, y, StateStatus.EMPTY)
        elif order.result is int(OrderResult.ship_hit):
            self.state_service.update_board(x, y, StateStatus.SHIP_HIT)
        elif order.result is int(OrderResult.ship_sunk):
            self.state_service.update_board(x, y, StateStatus.SHIP_SUNK)

    def _handle_recon_action(self, order):
        x = order.coordinate.x
        y = order.coordinate.y
        if order.result is int(OrderResult.ship_found):
            if self.state_service.state.board[y][x] is StateStatus.UNKNOWN:
                self.state_service.update_board(x, y, StateStatus.SHIP, persist=False)
            self._set_recon_empty_coords(order.coordinate)
        elif order.result is int(OrderResult.ship_not_found):
            self._set_recon_empty_coords(order.coordinate)

    def _set_recon_empty_coords(self, found_coordinate):
        recon_coordinate = self.state_service.state.recon_coordinate
        for x in range(recon_coordinate.x, recon_coordinate.x + 3):
            for y in range(recon_coordinate.y, recon_coordinate.y + 3):
                if x == found_coordinate.x and y == found_coordinate.y:
                    break
                self.state_service.update_board(x, y, StateStatus.EMPTY, persist=False)
            else:
                continue
            break
        self.state_service.persist_state()
