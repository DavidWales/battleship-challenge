from enum import IntEnum

class Orientation(IntEnum):
    vertical = 0
    horizontal = 1
