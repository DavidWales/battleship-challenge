from enum import IntEnum

class StateStatus(IntEnum):
    UNKNOWN = 9,
    EMPTY = 0,
    SHIP = 1,
    SHIP_HIT = 2,
    SHIP_SUNK = 3
