from flask import jsonify, request
from application import app
from .services import StateService, OrderService, GameUpdateService, FleetService
from .models import deserialize_order, deserialize_game_info

@app.route("/fleet")
def get_fleet():
    fleet_service = FleetService()
    fleet = fleet_service.get_fleet()
    return jsonify(fleet.serialize())

@app.route("/gameinfo", methods=["POST"])
def handle_game_info():
    data = request.get_json()
    info = deserialize_game_info(data)
    state_service = StateService(game_info=info)
    state_service.set_fleet()
    return jsonify(success=True)

@app.route("/gameupdates", methods=["POST"])
def handle_game_update():
    data = request.get_json()
    order = deserialize_order(data)
    game_update_service = GameUpdateService()
    game_update_service.update(order)
    return jsonify(success=True)

@app.route("/nextorder")
def get_order():
    order_service = OrderService()
    next_order = order_service.get_next_order()
    return jsonify(next_order.serialize())

@app.route("/state")
def get_state_overview():
    state_service = StateService()
    return state_service.state.to_html()
