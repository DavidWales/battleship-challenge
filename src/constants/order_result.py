from enum import IntEnum

class OrderResult(IntEnum):
    illegal_order = 0
    illegal_order_ship_sunk = 1
    order_not_executed = 2
    miss = 3
    ship_hit = 4
    ship_sunk = 5
    ship_found = 6
    ship_not_found = 7
    message_sent = 8
