import os
from random import random, randint
import numpy as np
from .state_service import StateService
from ..constants import Orientation, StateStatus

class SimulatorService():
    SHIP_MASK = np.full((20, 20), StateStatus.SHIP)

    def __init__(self):
        self.state_service = StateService()

    def get_board_probabilities(self, board):
        num_simulated_boards = int(os.getenv("SIM_COUNT"))
        ship_sizes = [5, 4, 4, 3, 2]
        simulated_boards = [self._generate_simulated_board(ship_sizes, board) \
            for i in range(num_simulated_boards)]
        probabilities = _calculate_probabilities(simulated_boards)
        return probabilities

    def _generate_simulated_board(self, ship_sizes, board):
        simmed_board = np.copy(board)
        for ship_size in ship_sizes:
            self._place_ship(ship_size, simmed_board)
        return np.equal(simmed_board, self.SHIP_MASK)

    def _place_ship(self, ship_size, board):
        orientation, x_coord, y_coord = self._get_ship_position(ship_size, board)
        for i in range(ship_size):
            x_offset = i if orientation is Orientation.horizontal else 0
            y_offset = i if orientation is Orientation.vertical else 0
            y = y_coord + y_offset
            x = x_coord + x_offset
            board[y, x] = StateStatus.SHIP if board[y, x] is not StateStatus.EMPTY \
                else StateStatus.EMPTY

    def _get_ship_position(self, ship_size, board):
        orientation = _get_random_orientation()
        #only retry 100 times before giving up
        available_tuples = np.argwhere(board != StateStatus.EMPTY)
        for _ in range(100):
            index = randint(0, len(available_tuples)-1)
            coord = available_tuples[index]
            if _validate_position(board, ship_size, orientation, coord[1], coord[0]):
                break
        return (orientation, coord[1], coord[0])

def _get_random_orientation():
    if random() > 0.5:
        return Orientation.vertical
    return Orientation.horizontal

def _validate_position(board, ship_size, orientation, x_coord, y_coord):
    if orientation is Orientation.vertical and y_coord > len(board) - ship_size - 1:
        return False
    if orientation is Orientation.horizontal and x_coord > len(board[0]) - ship_size - 1:
        return False
    for i in range(ship_size):
        x_offset = i if orientation is Orientation.horizontal else 0
        y_offset = i if orientation is Orientation.vertical else 0
        if board[y_coord + y_offset, x_coord + x_offset] is StateStatus.EMPTY:
            return False
    return True

def _calculate_probabilities(simulated_boards):
    return np.mean(simulated_boards, axis=0)
