import os
from redis import StrictRedis

class StorageService:
    def __init__(self):
        self.client = StrictRedis(
            host='wales-battleship-challenge.redis.cache.windows.net',
            port=6380,
            db=int(os.getenv("REDIS_DB"), 0),
            password=os.getenv("REDIS_ACCESS_TOKEN"),
            ssl=True
        )

    def get_val(self, key: str):
        return self.client.get(key)

    def set_val(self, key: str, val: object):
        self.client.set(key, val)
