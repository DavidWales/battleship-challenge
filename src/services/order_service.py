import numpy as np
from .storage_service import StorageService
from .state_service import StateService
from .simulator_service import SimulatorService
from ..models import Order, Coordinate
from ..constants import OrderType, OrderResult, StateStatus

class OrderService():
    attack_threshold = 0.5

    def __init__(self):
        self.storage_service = StorageService()
        self.state_service = StateService()
        self.simulator_service = SimulatorService()

    def get_next_order(self):
        next_order = Order()
        player_info, player_board = self._get_target()
        if player_info is None:
            return self._get_gloat()
        action_type, attack_position_local = self._get_attack(player_board)
        attack_position_world = _local_to_world_point(player_info, attack_position_local)
        next_order.order_type = action_type
        next_order.coordinate = attack_position_world
        next_order.player_targeted = player_info.player_name
        next_order.result = OrderResult.order_not_executed
        if action_type is OrderType.recon:
            self.state_service.state.recon_coordinate = attack_position_world
            self.state_service.persist_state()
        return next_order

    def _get_target(self):
        for player_info in self.state_service.state.game_info.players:
            player_board = self._get_player_board(player_info)
            is_not_self = player_info.player_name != self.state_service.state.fleet_name
            has_unknown = any(StateStatus.UNKNOWN in row for row in player_board)
            if is_not_self and has_unknown:
                return player_info, player_board
        return None, None

    def _get_player_board(self, player_info):
        start = player_info.start_coordinate
        end = player_info.end_coordinate
        return [row[start.x:end.x+1] for row in self.state_service.state.board[start.y:end.y+1]]

    def _get_attack(self, player_board):
        probability_distribution = self.simulator_service.get_board_probabilities(player_board)
        self._adjust_probs(player_board, probability_distribution)
        (attack_prob, attack_index) = self._max_attack(probability_distribution)
        (recon_prob, recon_index) = self._max_recon(player_board, probability_distribution)
        if attack_prob / 2 >= recon_prob or attack_prob > self.attack_threshold:
            attack_type = OrderType.attack
            index = attack_index
        else:
            attack_type = OrderType.recon
            index = recon_index
        return (attack_type, Coordinate(int(index[1]), int(index[0])))

    def _max_attack(self, probability_distribution):
        board_shape = np.shape(probability_distribution)
        index = np.unravel_index(np.argmax(probability_distribution), board_shape)
        return (probability_distribution[index], index)

    def _max_recon(self, board, probability_distribution):
        board_shape = np.shape(probability_distribution)
        block_sums = [[self._block_sum(board, probability_distribution, i, j) \
            for j in range(len(probability_distribution[i]))] \
                for i in range(len(probability_distribution))]
        index = np.unravel_index(np.argmax(block_sums), board_shape)
        return (np.array(block_sums)[index], index)

    def _block_sum(self, board, probs, start_i, start_j):
        end_i = min(len(probs), start_i + 3)
        end_j = min(len(probs[0]), start_j + 3)
        result = 0
        for i in range(start_i, end_i):
            for j in range(start_j, end_j):
                if board[i][j] in [StateStatus.SHIP, StateStatus.SHIP_HIT, StateStatus.SHIP_SUNK]:
                    return result
                result += probs[i][j]
        return result

    def _adjust_probs(self, original_board, prob_board):
        for i, _ in enumerate(original_board):
            for j, _ in enumerate(original_board[i]):
                if original_board[i][j] in [StateStatus.EMPTY, StateStatus.SHIP_HIT, StateStatus.SHIP_SUNK]:
                    prob_board[i][j] = 0.0
                elif original_board[i][j] in [StateStatus.SHIP]:
                    prob_board[i][j] = 1.0

    def _get_gloat(self):
        gloat_order = Order()
        gloat_order.data = "Victory is mine!"
        gloat_order.player_targeted = self.state_service.state.game_info.players[0].player_name
        gloat_order.order_type = OrderType.broadcast_message
        gloat_order.result = OrderResult.order_not_executed
        return gloat_order

def _local_to_world_point(player_info, coord):
    return Coordinate(coord.x + player_info.start_coordinate.x, \
        coord.y + player_info.start_coordinate.y)
