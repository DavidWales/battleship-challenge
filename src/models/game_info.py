from .player_info import PlayerInfo, deserialize_player_info

class GameInfo():
    players: PlayerInfo = []
    battlefield_width: int = None
    battlefield_height: int = None

    def __str__(self):
        return f"<GameInfo [{[p.__str__() for p in self.players]}] {self.battlefield_width} \
            {self.battlefield_height}>"

    def deserialize(self, data):
        players_json = data.get("players")
        self.players = [deserialize_player_info(pj) for pj in players_json]
        self.battlefield_height = data.get("BattleFieldHeight")
        self.battlefield_width = data.get("BattleFieldWidth")

def deserialize_game_info(data):
    game_info = GameInfo()
    game_info.deserialize(data)
    return game_info
