from .coordinate import Coordinate, deserialize_coordinate

class Order():
    order_type: int = None
    coordinate: Coordinate = None
    data: str = None
    result: int = None
    player_targeted: str = None

    def __str__(self):
        return f"<Order {self.order_type} {self.coordinate.__str__()} {self.data} {self.result} \
            {self.player_targeted}>"

    def serialize(self):
        return {
            "Type": self.order_type,
            "Coordinate": self.coordinate.serialize(),
            "Data": self.data,
            "Result": self.result,
            "PlayerTargeted": self.player_targeted
        }

    def deserialize(self, data):
        self.order_type = data.get("Type")
        self.coordinate = deserialize_coordinate(data.get("Coordinate", {}))
        self.data = data.get("Data", None)
        self.result = data.get("Result")
        self.player_targeted = data.get("PlayerTargeted", None)

def deserialize_order(data):
    order = Order()
    order.deserialize(data)
    return order
